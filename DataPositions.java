package jpp.qrcode;

public class DataPositions {
    private int i;
    private int j;
    private boolean goingUp = true;
    private ReservedModulesMask mask;
    private boolean right = true;

	public DataPositions(ReservedModulesMask mask) {
		this.mask = mask;
		i = mask.size() - 1;
		j = mask.size() - 1;
	}
	
	public int i() {
	    return i;
	}
	
	public int j() {
		return j;
	}
	
	public boolean next() {
	    if (right) {
            right = false;
	        j--;
            if (j < 0) {
                return false;
            }
	        if (mask.isReserved(i, j)) {
	            return next();
            } else {
	            return true;
            }
        } else {
            right = true;
	        j++;

	        return nextBig();
        }
	}

	private boolean nextBig() {
	    if (goingUp) {
	        i--;
	        checkBoundaries();
            if (j < 0) {
                return false;
            }
	        if (mask.isReserved(i, j)) {
	            return next();
            } else {
	            return true;
            }
        } else {
	        i++;
            checkBoundaries();
            if (j < 0) {
                return false;
            }
	        if(mask.isReserved(i, j)) {
	            return next();
            } else {
	            return true;
            }
        }
    }

    private void checkBoundaries() {
        if (i < 0 || i >= mask.size()) {
            goingUp ^= true;
            j -= 2;
            if (j == 6) {
                j--;
            }

            if (goingUp) {
                i = mask.size() - 1;
            } else {
                i = 0;
            }
        }
    }
}
