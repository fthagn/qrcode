package jpp.qrcode.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class TextWriter {
	public static void write(OutputStream out, boolean[][] data) throws IOException {
        PrintStream ps = new PrintStream(out);
		for (boolean[] row: data) {
		    for (boolean b: row) {
                if (b) {
                    ps.print('1');
                } else {
                    ps.print('0');
                }
            }
		    ps.print('\n');
        }
	}
}
