package jpp.qrcode.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TextReader {
	public static boolean[][] read(InputStream in) throws IOException {

        ArrayList<boolean[]> matrix = new ArrayList<>();
        ArrayList<Boolean> row = new ArrayList<>();
        for (int b; ((b = in.read()) >= 0);) {
            char c =  (char) b;
            switch (c) {
                case '#':
                    while (c != '\n') {
                        c = (char) in.read();
                    }
                    row = new ArrayList<>();
                    break;
                case '\n':
                    if (row.size() > 0) {
                        matrix.add(parseRow(row));
                    }
                    row = new ArrayList<>();
                    break;
                case '1':
                    row.add(true);
                    break;
                case '0':
                    row.add(false);
                    break;
                case ' ':
                    break;
                case '\t':
                    break;
                default:
                    throw new IOException("unknown symbol: " + c);

            }
        }
        matrix.add(parseRow(row));

        boolean[][] out = new boolean[matrix.size()][];
        for (int i = 0; i < matrix.size(); i++) {
            out[i] = matrix.get(i);
        }
        return out;
	}

	private static boolean[] parseRow(ArrayList<Boolean> in) {
	    boolean[] out = new boolean[in.size()];
	    for (int i = 0; i < in.size(); i++) {
	        out[i] = in.get(i);
        }
	    return out;
    }
}
