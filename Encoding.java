package jpp.qrcode;

import java.awt.*;

public enum Encoding {
	NUMERIC, ALPHANUMERIC, BYTE, KANJI, ECI, INVALID;

	private int encodingBits;

	static {
        NUMERIC.encodingBits = 1;
        ALPHANUMERIC.encodingBits = 2;
        BYTE.encodingBits = 4;
        KANJI.encodingBits = 8;
        ECI.encodingBits = 7;
        INVALID.encodingBits = -1;
    }
	
	public static Encoding fromBits(int i) {
		switch (i) {
            case 1: return NUMERIC;
            case 2: return ALPHANUMERIC;
            case 4: return BYTE;
            case 8: return KANJI;
            case 7: return ECI;
            default: return INVALID;
        }

	}
	
	public int bits() {
	    return encodingBits;
	}
}
