package jpp.qrcode;

public class InvalidQRCodeException extends QRCodeException {
    public InvalidQRCodeException(String message) {
        super(message);
    }
}
