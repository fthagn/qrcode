package jpp.qrcode;

public class QRCodeException extends RuntimeException {
    public QRCodeException(String message) {
        super(message);
    }
}
