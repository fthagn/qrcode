package jpp.qrcode;

public class QRCode {
    private boolean[][] matrix;
    private Version version;
    private MaskPattern pattern;
    private ErrorCorrection correction;

    public QRCode(boolean[][] validatedQrCode, Version version, MaskPattern pattern, ErrorCorrection correction) {
        this.matrix = validatedQrCode;
        this.version = version;
        this.pattern = pattern;
        this.correction = correction;
    }

    public boolean[][] data() {
        return matrix;
    }

    public Version version() {
        return version;
    }

    public MaskPattern maskPattern() {
        return pattern;
    }

    public ErrorCorrection errorCorrection() {
        return correction;
    }

    public String matrixToString() {
        StringBuilder result = new StringBuilder();
        char black = 0x2588;
        char white = 0x2591;
        for (int i = 0; i < matrix.length; i++) {
            boolean[] row = matrix[i];
            for (boolean b: row) {
                if (b) {
                    result.append(black);
                    result.append(black);
                } else {
                    result.append(white);
                    result.append(white);
                }
            }
            result.append('\n');
        }
        result.setLength(result.length() - 1);
        return result.toString();
	}
	
	public static QRCode createValidatedFromBooleans(boolean[][] matrix) throws InvalidQRCodeException {
        //check basic conditions
		if (matrix == null) {
		    throw new InvalidQRCodeException("matrix is null");
        }
		if (matrix.length == 0) {
		    throw new InvalidQRCodeException("matrix is empty");
        }
		int width = matrix.length;
		for (boolean[] row: matrix) {
		    if (row.length != width) {
		        throw new InvalidQRCodeException("matrix is not square");
            }
        }

		int versionNr = -1;
		for (int i = 1; i <= 40; i++) {
		    if (width == 17 + i * 4) {
		        versionNr = i;
		        break;
            }
        }

		if (versionNr == -1) {
            throw new InvalidQRCodeException("no matching qr version found for width: " + width);
        }

		//check orientation patterns
        checkOrientation(matrix, 0, 0);
		checkOrientation(matrix, 0, width - 7);
		checkOrientation(matrix, width - 7, 0);

		//check orientation pattern margins
		for (int i = 0; i < 8; i++) {
		    if (matrix[7][i] || matrix[7][width - 8 + i] || matrix[width - 8][i]) {
		        throw new InvalidQRCodeException("orientation pattern margin invalid");
            }
        }
        for (int i = 0; i < 7; i++) {
            if (matrix[i][7] || matrix[width - 7 + i][7] || matrix[i][width - 8]) {
                throw new InvalidQRCodeException("orientation pattern margin invalid");
            }
        }

        //check timing patterns
        boolean pattern = true;
        for (int i = 0; i < width - 2 * 8; i++) {
            if (matrix[6][i + 8] != pattern) {
                throw new InvalidQRCodeException("horizontal timing pattern invalid");
            }
            pattern ^= true;
        }
        pattern = true;
        for (int i = 0; i < width - 2 * 8; i++) {
            if (matrix[i + 8][6] != pattern) {
                throw new InvalidQRCodeException("vertical timing pattern invalid");
            }
            pattern ^= true;
        }

        //check dark pattern
        if (!matrix[width - 8][8]) {
            throw new InvalidQRCodeException("dark module invalid");
        }

        //check version blocks
        Version version = Version.fromNumber(versionNr);
        if (versionNr > 6) {

            //bottom
            StringBuilder vNr = new StringBuilder();
            for (int j = 0; j < 6; j++) {
                for (int i = width - 11; i < width - 8; i++) {
                    if (matrix[i][j]) {
                        vNr.append('1');
                    } else {
                        vNr.append('0');
                    }
                }
            }
            vNr.reverse();

            version = VersionInformation.fromBits(Integer.parseInt(vNr.toString(), 2));
            if (version == null) {
                version = checkVersionRight(matrix, width, versionNr);
            }
            if (version.number() != versionNr) {
                version = checkVersionRight(matrix, width, versionNr);
           }
        }

        //check format infos
        StringBuilder formatNr = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            if (matrix[i][8]) {
                formatNr.append('1');
            } else {
                formatNr.append('0');
            }
        }

        if (matrix[7][8]) {
            formatNr.append('1');
        } else {
            formatNr.append('0');
        }

        if (matrix[8][8]) {
            formatNr.append('1');
        } else {
            formatNr.append('0');
        }

        if (matrix[8][7]) {
            formatNr.append('1');
        } else {
            formatNr.append('0');
        }

        for (int i = 0; i < 6; i++) {
            if (matrix[8][5 - i]) {
                formatNr.append('1');
            } else {
                formatNr.append('0');
            }
        }
        formatNr.reverse();

        FormatInformation format = FormatInformation.fromBits(Integer.parseInt(formatNr.toString(), 2));
        if (format == null) {
            formatNr = new StringBuilder();
            for (int i = 0; i < 8; i++) {
                if (matrix[8][width - 1 - i]) {
                    formatNr.append('1');
                } else {
                    formatNr.append('0');
                }
            }
            for (int i = 0; i < 7; i++) {
                if (matrix[width - 7 + i][8]) {
                    formatNr.append('1');
                } else {
                    formatNr.append('0');
                }
            }

            formatNr.reverse();

            format = FormatInformation.fromBits(Integer.parseInt(formatNr.toString(), 2));

            if (format == null) {
                throw new InvalidQRCodeException("format info invalid");
            }
        }

        //check alignment patterns
        int[] pos = version.alignmentPositions();
        for (int i = 0; i < pos.length; i++) {
            for (int j = 0; j < pos.length; j++) {
                if (!((i == 0 && j == 0) || (i == 0 && j == pos.length - 1) || (i == pos.length - 1 && j == 0))) {
                    checkAlignment(matrix, pos[i], pos[j]);
                }
            }
        }

        return new QRCode(matrix, version, format.maskPattern(), format.errorCorrection());
	}

	private static Version checkVersionRight(boolean[][] matrix, int width, int versionNr) {
        StringBuilder vNr = new StringBuilder();
        for (int i = 0; i < 6; i++) {
          for (int j = width - 11; j < width - 8; j++) {
                if (matrix[i][j]) {
                    vNr.append('1');
                } else {
                    vNr.append('0');
                }
            }
        }
        vNr.reverse();

        Version version = VersionInformation.fromBits(Integer.parseInt(vNr.toString(), 2));
        if (version == null) {
            throw new InvalidQRCodeException("invalid version block");
        }
        if (version.number() != versionNr) {
            throw new InvalidQRCodeException("invalid version block");
        }

        return version;
    }

	private static void checkAlignment(boolean[][] matrix, int row, int col) {
        if (!(checkSquare(matrix, true, row - 2, col - 2, 5)
                && checkSquare(matrix, false, row - 1, col - 1, 3)
                && checkSquare(matrix, true, row, col, 1))) {
            throw new InvalidQRCodeException("alignment pattern invalid");
        }
    }

    private static void checkOrientation(boolean[][] matrix, int row, int col) {
        if (!(checkSquare(matrix, true, row, col, 7)
                && checkSquare(matrix, false, row + 1, col + 1, 5)
                && checkSquare(matrix, true, row + 2, col + 2, 3)
                && checkSquare(matrix, true, row + 3, col + 3, 1))) {
            throw new InvalidQRCodeException("orientation pattern invalid");
        }
    }

    private static boolean checkSquare(boolean[][] matrix, boolean color, int row, int col, int width) {
        //System.out.println("color: " + color + ", row: " + row + ", col: " + col + ", width: " + width);
        if (width == 1) {
            return matrix[row][col] == color;
        }

        //check horizontal lines
        for (int i = col; i < col + width; i++) {
            if (matrix[row][i] != color || matrix[row + width - 1][i] != color) {
                return false;
            }
        }

        //check vertical lines
        for (int i = row + 1; i < row + width - 1; i++) {
            if (matrix[i][col] != color || matrix[i][col + width - 1] != color) {
                return false;
            }
        }
        return true;
    }
}
