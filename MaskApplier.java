package jpp.qrcode;

public class MaskApplier {
	public static void applyTo(boolean[][] matrix, MaskFunction mask, ReservedModulesMask reserved) {
	    if (matrix.length != reserved.size()) {
	        throw new IllegalArgumentException("incompatible mask size");
        }

		for (int i = 0; i < matrix.length; i++) {
		    for (int j = 0; j < matrix.length; j++) {
		        if (mask.mask(i, j) && !reserved.isReserved(i, j)) {
                    matrix[i][j] ^= true;
                }
            }
        }
	}
}
