package jpp.qrcode.encode;

import jpp.qrcode.DataPositions;
import jpp.qrcode.ReservedModulesMask;

public class DataInserter {
	public static void insert(boolean[][] target, ReservedModulesMask mask, byte[] data) {
	    DataPositions pos = new DataPositions(mask);

		for (byte d: data) {
            String binary = Integer.toBinaryString(d);

            for (int i = binary.length() - 8; i < binary.length(); i++) {
                if (i > -1) {
                    if (binary.charAt(i) == '1') {
                        target[pos.i()][pos.j()] = true;
                    }
                }
                pos.next();
            }
        }
	}
}
