package jpp.qrcode.encode;

import jpp.qrcode.DataBlock;
import jpp.qrcode.ErrorCorrectionGroup;
import jpp.qrcode.ErrorCorrectionInformation;
import jpp.qrcode.reedsolomon.ReedSolomon;

public class DataStructurer {
	public static DataBlock[] split(byte[] data, ErrorCorrectionInformation errorCorrectionInformation) {
	    int totalBlockCount = errorCorrectionInformation.totalBlockCount();
	    int correctionBytesPerBlock = errorCorrectionInformation.correctionBytesPerBlock();
		ErrorCorrectionGroup[] correctionGroups = errorCorrectionInformation.correctionGroups();

		DataBlock[] dataBlocks = new DataBlock[totalBlockCount];
		int lastDataBlockIndex = 0;
		int uncopiedDataIndex = 0;

		for (ErrorCorrectionGroup ecGroup: correctionGroups) {
		    int blockCount = ecGroup.blockCount();
		    int dataByteCount = ecGroup.dataByteCount();

		    for (int i = 0; i < blockCount; i++) {
		        byte[] dataBytes = new byte[dataByteCount];
                System.arraycopy(data, uncopiedDataIndex, dataBytes, 0, dataByteCount);
                uncopiedDataIndex += dataByteCount;
                byte[] correctionBytes = ReedSolomon.calculateCorrectionBytes(dataBytes, correctionBytesPerBlock);
                dataBlocks[lastDataBlockIndex++] = new DataBlock(dataBytes, correctionBytes);
            }
        }

	    return dataBlocks;
	}
	
	public static byte[] interleave(DataBlock[] blocks, ErrorCorrectionInformation ecBlocks) {
		byte[] mixed = new byte[ecBlocks.totalByteCount()];
		int resultIndex = 0;

		//add data bytes
        boolean done = false;
        int byteIndex = 0;
        while(!done) {
            int doneBlocks = 0;
            for (DataBlock block: blocks) {
                byte[] dataBytes = block.dataBytes();
                if (byteIndex < dataBytes.length) {
                    mixed[resultIndex++] = dataBytes[byteIndex];
                } else {
                    doneBlocks++;
                }
            }
            byteIndex++;
            if (doneBlocks >= blocks.length) {
                done = true;
            }
        }

        //add correction bytes
        done = false;
        byteIndex = 0;
        int correctionBytesPerBlock = ecBlocks.correctionBytesPerBlock();
        while(!done) {
            for (DataBlock block: blocks) {
                byte[] correctionBytes = block.correctionBytes();
                mixed[resultIndex++] = correctionBytes[byteIndex];

            }
            if (++byteIndex >= correctionBytesPerBlock) {
                done = true;
            }
        }

		return mixed;
	}
	
	public static byte[] structure(byte[] data, ErrorCorrectionInformation ecBlocks) {
		if (data.length != ecBlocks.totalDataByteCount()) {
		    throw new IllegalArgumentException("unexpected data byte length");
        }

		return interleave(split(data, ecBlocks), ecBlocks);
	}
}
