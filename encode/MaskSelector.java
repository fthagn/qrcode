package jpp.qrcode.encode;

import jpp.qrcode.*;

public class MaskSelector {
	public static void placeFormatInformation(boolean[][] res, int formatInformation) {
        String binary = Integer.toBinaryString(formatInformation);
        boolean[] b = new boolean[15];

        int offset = 15 - binary.length();
        for (int i = offset; i < 15; i++) {
            if (binary.charAt(i - offset) == '1') {
                b[14 - i] = true;
            }
        }

        //upper left
        for (int i = 0; i < 6; i++) {
            res[i][8] = b[i];
        }
        res[7][8] = b[6];
        res[8][8] = b[7];
        res[8][7] = b[8];
        for (int i = 0; i < 6; i++) {
            res[8][5 - i] = b[9 + i];
        }

        //right
        for (int i = 0; i < 8; i++) {
            res[8][res.length - 1 - i] = b[i];
        }

        //bottom
        for (int i = 0; i < 7; i++) {
            res[res.length - 7 + i][8] = b[8 + i];
        }

	}
	
	public static int calculatePenaltySameColored(boolean[][] data) {
		int penalty = 0;

		for (int i = 0; i < data.length; i++) {
            int horCnt = 1;
            int verCnt = 1;

            boolean horClr = data[i][0];
            boolean verClr = data[0][i];
		    for (int j = 1; j < data.length; j++) {

                if (data[i][j] == horClr) {
                    horCnt++;
                    if (horCnt == 5) {
                        penalty += 3;
                    } else if (horCnt > 5) {
                        penalty++;
                    }
                } else {
                    horCnt = 1;
                    horClr ^= true;
                }

                if (data[j][i] == verClr) {
                    verCnt++;
                    if (verCnt == 5) {
                        penalty += 3;
                    } else if (verCnt > 5) {
                        penalty++;
                    }
                } else {
                    verCnt = 1;
                    verClr ^= true;
                }
            }
        }

		return penalty;
	}
	
	public static int calculatePenalty2x2(boolean[][] data) {
		int penalty = 0;

		for (int i = 0; i < data.length - 1; i++) {
		    for (int j = 0; j < data.length - 1; j++) {
                boolean b0 = data[i][j];
                boolean b1 = data[i + 1][j];
                boolean b2 = data[i][j + 1];
                boolean b3 = data[i + 1][j + 1];
		        if ((b0==b1)&&(b1==b2)&&(b2==b3)) {
		            penalty += 3;
                }
            }
        }

		return penalty;
	}

	public static int calculatePenaltyPattern(boolean[][] array) {
	    int penalty = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 6; j++) {
                if (checkBlackWhitePatternHor(array, i, j)) {
                    penalty += 40;
                }
            }
        }

        for (int i = 0; i < array.length - 6; i++) {
            for (int j = 0; j < array.length; j++) {
                if (checkBlackWhitePatternVer(array, i, j)) {
                    penalty += 40;
                }
            }
        }

		return penalty;
	}

    private static boolean checkBlackWhitePatternHor(boolean[][] array, int i, int j) {
        if (array[i][j] &&
                (!array[i][j + 1]) &&
                array[i][j + 2] &&
                array[i][j + 3] &&
                array[i][j + 4] &&
                (!array[i][j + 5]) &&
                array[i][j + 6]) {
            return countWhitesHor(array, i, j - 4) || countWhitesHor(array, i, j + 7);
        }
        return false;
    }

    private static boolean checkBlackWhitePatternVer(boolean[][] array, int i, int j) {
        if (array[i][j] &&
                (!array[i + 1][j]) &&
                array[i + 2][j] &&
                array[i + 3][j] &&
                array[i + 4][j] &&
                (!array[i + 5][j]) &&
                array[i + 6][j]) {
            return countWhitesVer(array, i - 4, j) || countWhitesVer(array, i + 7, j);
        }
        return false;
    }

    private static boolean countWhitesHor(boolean[][] array, int i, int j) {
        if (j + 4 > array.length || j < 0) {
            return false;
        }
        if ((!array[i][j + 0]) &&
                (!array[i][j + 1]) &&
                (!array[i][j + 2]) &&
                (!array[i][j + 3])) {
            return true;
        }
        return false;
    }

    private static boolean countWhitesVer(boolean[][] array, int i, int j) {
        if (i + 4 > array.length || i < 0) {
            return false;
        }
        if ((!array[i][j]) &&
                (!array[i + 1][j]) &&
                (!array[i + 2][j]) &&
                (!array[i + 3][j])) {
            return true;
        }
        return false;
    }
	
	public static int calculatePenaltyBlackWhite(boolean[][] data) {
		int b = 0;

		for (boolean[] row: data) {
		    for (boolean d: row) {
		        if (d) {
		            b++;
                }
            }
        }

		int t = data.length * data.length;

		return 10 * (Math.abs(2 * b - t) * 10 / t);
	}
	
	public static int calculatePenaltyFor(boolean[][] data) {
		return calculatePenalty2x2(data) + calculatePenaltyBlackWhite(data)
                + calculatePenaltyPattern(data) + calculatePenaltySameColored(data);
	}
	
	public static MaskPattern maskWithBestMask(boolean[][] data, ErrorCorrection correction, ReservedModulesMask modulesMask) {
        if (data.length != modulesMask.size()) {
            throw new IllegalArgumentException("incompatible sizes of data and modulesMask");
        }


	    MaskPattern[] masks = MaskPattern.values();
	    int[] penalties = new int[masks.length];

        for (int i = 0; i < masks.length; i++) {
            MaskApplier.applyTo(data, masks[i].maskFunction(), modulesMask);
            placeFormatInformation(data, FormatInformation.get(correction, masks[i]).formatInfo());
            penalties[i] = calculatePenaltyFor(data);
            MaskApplier.applyTo(data, masks[i].maskFunction(), modulesMask);
        }

        int minP = penalties[0];
        int minPi = 0;

        for (int i = 1; i < penalties.length; i++) {
            if (penalties[i] < minP) {
                minP = penalties[i];
                minPi = i;
            }
        }

        MaskApplier.applyTo(data, masks[minPi].maskFunction(), modulesMask);
        placeFormatInformation(data, FormatInformation.get(correction, masks[minPi]).formatInfo());

        return masks[minPi];
	}
}
