package jpp.qrcode.encode;

import jpp.qrcode.Encoding;
import jpp.qrcode.ErrorCorrection;
import jpp.qrcode.ErrorCorrectionInformation;
import jpp.qrcode.Version;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public final class DataEncoder {
	public static DataEncoderResult encodeForCorrectionLevel(String str, ErrorCorrection level) {

	    int offset = 1;
        int characterCount = str.length();
	    Version version = Version.forDataBytesCount(characterCount + 1 + offset, level);
	    if (version.number() >= 10) {
	        offset = 2;
            version = Version.forDataBytesCount(characterCount + 1 + offset, level);
        }

        int dataByteCount = version.correctionInformationFor(level).totalDataByteCount();
	    byte[] bytes = new byte[dataByteCount];
	    bytes[0] += Encoding.BYTE.bits() * 16;

	    if (version.number() <= 9) {
	        bytes[0] += characterCount / 16;
	        bytes[1] += (characterCount % 16) * 16;
        } else {
            bytes[0] += characterCount / 4096;
            bytes[1] += (characterCount / 16) % 256;
            bytes[2] += (characterCount % 16) * 16;
        }

        Charset iso88591charset = Charset.forName("ISO-8859-1");
	    ByteBuffer strBytes = iso88591charset.encode(str);
	    for (int i = 0; i < characterCount; i++) {
            byte b = strBytes.get();
            bytes[i + offset] += b / 16;
            bytes[i + offset + 1] += (b % 16) * 16;
        }

	    //padding
        boolean padWith236 = true;
        for (int i = characterCount + offset + 1; i < dataByteCount; i++) {
            if (padWith236) {
                bytes[i] = (byte) 236;
            } else {
                bytes[i] = (byte) 17;
            }
            padWith236 ^= true;
        }

	    return new DataEncoderResult(bytes, version);
	}
}
