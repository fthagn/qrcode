package jpp.qrcode.encode;

import jpp.qrcode.Version;
import jpp.qrcode.VersionInformation;

public class PatternPlacer {
	public static void placeOrientation(boolean[][] res, Version version) {
	    placeSingleOrientation(res, 0, 0);
	    placeSingleOrientation(res, version.size() - 7, 0);
	    placeSingleOrientation(res, 0, version.size() - 7);
	}
	
	public static void placeTiming(boolean[][] res, Version version) {
		for (int i = 8; i < version.size() - 8; i += 2) {
		    res[6][i] = true;
		    res[i][6] = true;
        }
	}
	
	public static void placeAlignment(boolean[][] res, Version version) {
        int[] pos = version.alignmentPositions();
        for (int i = 0; i < pos.length; i++) {
            for (int j = 0; j < pos.length; j++) {
                if (!((i == 0 && j == 0) || (i == 0 && j == pos.length - 1) || (i == pos.length - 1 && j == 0))) {
                    placeSingleAlignment(res, pos[i], pos[j]);
                }
            }
        }
	}
	
	public static void placeVersionInformation(boolean[][] data, int versionInformation) {
        String binary = Integer.toBinaryString(versionInformation);
        binary = new StringBuilder(binary).reverse().toString();
        System.out.println(binary);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 6; j++) {
                int idx = i + j * 3;
                if (idx < binary.length()) {
                    if (binary.charAt(i + j * 3) == '1') {
                        data[j][data.length - 11 + i] = true;
                        data[data.length - 11 + i][j] = true;
                    }
                }
            }
        }
	}
	
	public static boolean[][] createBlankForVersion(Version version) {
		boolean[][] matrix = new boolean[version.size()][version.size()];

		//place dark module
        matrix[version.size() - 8][8] = true;

		placeOrientation(matrix, version);
		placeTiming(matrix, version);
		placeAlignment(matrix, version);

        if (version.number() > 6) {
            placeVersionInformation(matrix, VersionInformation.forVersion(version));
        }

		return matrix;
	}

	private static void placeSingleAlignment(boolean[][] matrix, int row, int col) {
	    placeSquare(matrix, row, col, 1);
	    placeSquare(matrix, row - 2, col - 2, 5);
    }

	private static void placeSingleOrientation(boolean[][] matrix, int row, int col) {
	    placeSquare(matrix, row, col, 7);
	    placeSquare(matrix, row + 2, col + 2, 3);
	    placeSquare(matrix, row + 3, col + 3, 1);
    }

	private static void placeSquare(boolean[][] matrix, int row, int col, int size) {
	    if (size == 1) {
	        matrix[row][col] = true;
	        return;
        }
	    placeRectangle(matrix, row, col, size, 1);
	    placeRectangle(matrix, row + size - 1, col, size, 1);
	    placeRectangle(matrix, row + 1, col, 1, size - 2);
	    placeRectangle(matrix, row + 1, col + size - 1, 1, size - 2);
    }

	private static void placeRectangle(boolean[][] matrix, int row, int col, int width, int height) {
	    for (int i = row; i < row + height; i++) {
	        for (int j = col; j < col + width; j++) {
	            matrix[i][j] = true;
            }
        }
    }
}
