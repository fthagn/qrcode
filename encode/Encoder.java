package jpp.qrcode.encode;

import jpp.qrcode.*;

public class Encoder {
	public static QRCode createFromString(String msg, ErrorCorrection correction) {
        DataEncoderResult encoded = DataEncoder.encodeForCorrectionLevel(msg, correction);

        Version version = encoded.version();
        ErrorCorrectionInformation eci = version.correctionInformationFor(correction);

        byte[] structured = DataStructurer.structure(encoded.bytes(), eci);

        boolean[][] matrix = PatternPlacer.createBlankForVersion(version);

        ReservedModulesMask reserved = ReservedModulesMask.forVersion(version);

        DataInserter.insert(matrix, reserved, structured);

        MaskPattern pattern = MaskSelector.maskWithBestMask(matrix, correction, reserved);

        System.out.println(pattern);


        QRCode newQRCode =  new QRCode(matrix, version, pattern, correction);
        return newQRCode;
	}
}
