package jpp.qrcode;

public class DataBlock {
    private byte[] dataBytes;
    private byte[] correctionBytes;

    //"Sie sollen keine Kopien zu erstellen" ? not language german parsings of being
	public DataBlock(byte[] dataBytes, byte[] correctionBytes) {
	    this.dataBytes = dataBytes;
	    this.correctionBytes = correctionBytes;
	}
	
	public byte[] dataBytes() {
	    return dataBytes;
	}
	
	public byte[] correctionBytes() {
	    return correctionBytes;
	}
}
