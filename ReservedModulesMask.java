package jpp.qrcode;

public class ReservedModulesMask {
    private final boolean[][] mask;

	public ReservedModulesMask(boolean[][] mask) {
	    this.mask = mask;
	}
	
	public boolean isReserved(int i, int j) {
	    return mask[i][j];
	}
	
	public int size() {
		return mask.length;
	}
	
	public static ReservedModulesMask forVersion(Version version) {
		boolean[][] matrix = new boolean[version.size()][version.size()];

		//add orientation and format masks
        addRectangle(matrix, 0, 0, 9, 9);
        addRectangle(matrix, version.size() - 8, 0, 9, 8);
        addRectangle(matrix, 0, version.size() - 8, 8, 9);

        //add alignment masks
        int[] pos = version.alignmentPositions();
        for (int i = 0; i < pos.length; i++) {
            for (int j = 0; j < pos.length; j++) {
                if (!((i == 0 && j == 0) || (i == 0 && j == pos.length - 1) || (i == pos.length - 1 && j == 0))) {
                    addRectangle(matrix,pos[i] - 2, pos[j] - 2, 5, 5);
                }
            }
        }

        //add timing masks
        addRectangle(matrix, 6, 0, version.size() - 1, 1);
        addRectangle(matrix, 0, 6, 1, version.size() - 1);

        //add version masks
        if (version.number() > 6) {
            addRectangle(matrix, version.size() - 11, 0, 6, 3);
            addRectangle(matrix, 0, version.size() - 11, 3, 6);
        }

        return new ReservedModulesMask(matrix);
	}

    private static void addRectangle(boolean[][] matrix, int row, int col, int width, int height) {
	    for (int i = row; i < row + height; i++) {
	        for (int j = col; j < col + width; j++) {
	            matrix[i][j] = true;
            }
        }
    }
}
