package jpp.qrcode.decode;

import jpp.qrcode.QRCodeException;

public class QRDecodeException extends QRCodeException {
    public QRDecodeException(String message) {
        super(message);
    }
}
