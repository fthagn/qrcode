package jpp.qrcode.decode;

import jpp.qrcode.DataPositions;
import jpp.qrcode.ReservedModulesMask;

public class DataExtractor {
	public static byte[] extract(boolean[][] matrix, ReservedModulesMask reservedModules, int byteCount) {
        if (reservedModules.size() != matrix.length) {
            throw new IllegalArgumentException("incompatible sizes of matrix and reservedModules");
        }

        DataPositions pos = new DataPositions(reservedModules);
        byte[] out = new byte[byteCount];
        int readBytes = 0;
        int readBits = 0;
        StringBuilder b = new StringBuilder();

        do {
            boolean bit = matrix[pos.i()][pos.j()];
            if (bit) {
                b.append('1');
            } else {
                b.append('0');
            }
            if (++readBits > 7) {
                readBits = 0;
                out[readBytes++] = (byte) Integer.parseInt(b.toString(), 2);
                b = new StringBuilder();
            }
            if (readBytes >= byteCount) {
                break;
            }
        } while (pos.next());

        return out;
	}
}
