package jpp.qrcode.decode;

import jpp.qrcode.Encoding;
import jpp.qrcode.ErrorCorrection;
import jpp.qrcode.Version;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

public class DataDecoder {
	public static Encoding readEncoding(byte[] bytes) {
	    int first4bits = bytes[0];
	    if (first4bits < 0) {
	        first4bits += 256;
        }
	    first4bits /= 16;
	    return Encoding.fromBits(first4bits);
	}
	
	public static int readCharacterCount(byte[] bytes, int bitCount) {
		int out = (posByte(bytes[0]) % 16) * 16 + posByte(bytes[1]) / 16;
		if (bitCount == 16) {
		    out *= 16 * 16;
		    out += (posByte(bytes[1]) % 16) * 16 + posByte(bytes[2]) / 16;
        }
		return out;
	}

	private static int posByte(byte b) {
	    if (b < 0) {
	        return b + 256;
        }
	    return b;
    }
	
	public static String decodeToString(byte[] bytes, Version version, ErrorCorrection errorCorrection) {
	    if (bytes.length != version.correctionInformationFor(errorCorrection).totalDataByteCount()) {
	        throw new IllegalArgumentException("inconsistent data byte counts");
        }

        Encoding encoding = readEncoding(bytes);
        if (encoding != Encoding.BYTE) {
            throw new QRDecodeException("encoding not allowed: " + encoding);
        }

        int countLength = 8;
        if (version.number() >= 10) {
            countLength = 16;
        }
        int characterCount = readCharacterCount(bytes, countLength);
        int offset = countLength / 8;
        if (characterCount > bytes.length - 1 - offset) {
            throw new QRDecodeException("incompatible character count for data size");
        }

        byte[] msg = new byte[characterCount];
        for (int i = 0; i < msg.length; i++) {
            int b = (posByte(bytes[i + offset]) % 16) * 16 +
                    posByte(bytes[i + offset + 1]) / 16;
            msg[i] = (byte) b;
        }

        Charset iso88591charset = Charset.forName("ISO-8859-1");
        CharsetDecoder decoder = iso88591charset.newDecoder();
        ByteBuffer bbuffer = ByteBuffer.wrap(msg);
        CharBuffer out;
        try {
            out = decoder.decode(bbuffer);
        } catch (CharacterCodingException e) {
            throw new QRDecodeException("decoding error: " + e.getMessage());
        }

        return out.toString();
	}
}
