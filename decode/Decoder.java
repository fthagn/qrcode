package jpp.qrcode.decode;

import jpp.qrcode.*;

public class Decoder {
	public static String decodeToString(QRCode qrCode) {
	    Version version = qrCode.version();
        ErrorCorrection errorCorrection = qrCode.errorCorrection();
        ErrorCorrectionInformation errorCorrectionInformation = version.correctionInformationFor(errorCorrection);
        int byteCount = version.totalByteCount();
        MaskPattern maskPattern = qrCode.maskPattern();
	    boolean[][] original = qrCode.data();

	    boolean[][] matrix = new boolean[version.size()][version.size()];

	    for (int i = 0; i < original.length; i++) {
	        matrix[i] = original[i].clone();
        }

	    ReservedModulesMask reserved = ReservedModulesMask.forVersion(version);

        MaskApplier.applyTo(matrix, maskPattern.maskFunction(), reserved);

        byte[] extracted = DataExtractor.extract(matrix, reserved, byteCount);

        byte[] destructured = DataDestructurer.destructure(extracted, errorCorrectionInformation);

        String decoded = DataDecoder.decodeToString(destructured, version, errorCorrection);

        return decoded;
	}
}
