package jpp.qrcode.decode;

import jpp.qrcode.DataBlock;
import jpp.qrcode.ErrorCorrectionGroup;
import jpp.qrcode.ErrorCorrectionInformation;
import jpp.qrcode.reedsolomon.ReedSolomon;
import jpp.qrcode.reedsolomon.ReedSolomonException;

public class DataDestructurer {
	public static byte[] join(DataBlock[] blocks, ErrorCorrectionInformation errorCorrectionInformation) {
        byte[] out = new byte[errorCorrectionInformation.totalDataByteCount()];
        int lastOutIndex = 0;

        for (DataBlock block: blocks) {
            byte[] dataBytes = block.dataBytes();
            byte[] correctionBytes = block.correctionBytes();

            try {
                ReedSolomon.correct(dataBytes, correctionBytes);
            } catch (ReedSolomonException e) {
                throw new QRDecodeException("ReedSolomonException: " + e.getMessage());
            }

            System.arraycopy(dataBytes, 0, out, lastOutIndex, dataBytes.length);
            lastOutIndex += dataBytes.length;
        }

	    return out;
	}
	
	public static DataBlock[] deinterleave(byte[] data, ErrorCorrectionInformation errorCorrectionInformation) {
	    int totalBlockCount = errorCorrectionInformation.totalBlockCount();
		DataBlock[] out = new DataBlock[totalBlockCount];
		int lastOutIndex = 0;

        int correctionByteCount = errorCorrectionInformation.correctionBytesPerBlock();

        int corrOffset = 0;
        for (ErrorCorrectionGroup group: errorCorrectionInformation.correctionGroups()) {
            corrOffset += group.dataByteCount() * group.blockCount();
        }

        ErrorCorrectionGroup[] groups = errorCorrectionInformation.correctionGroups();
        for (int gNum = 0; gNum < groups.length; gNum++) {
            ErrorCorrectionGroup group = groups[gNum];
            int dataByteCount = group.dataByteCount();
            for (int i = 0; i < group.blockCount(); i++) {
                byte[] dataBytes = new byte[dataByteCount];
                byte[] correctionBytes = new byte[correctionByteCount];

                for (int j = 0; j < dataBytes.length; j++) {
                    int idx = lastOutIndex + j * totalBlockCount;
                    if (gNum > 0 && j == dataBytes.length - 1) {
                        idx -= groups[0].blockCount();
                    }
                    dataBytes[j] = data[idx];
                }

                for (int j = 0; j < correctionBytes.length; j++) {
                    correctionBytes[j] = data[corrOffset + lastOutIndex + j * totalBlockCount];
                }

                out[lastOutIndex++] = new DataBlock(dataBytes, correctionBytes);
            }
        }

		return out;

	}
	
	public static byte[] destructure(byte[] data, ErrorCorrectionInformation ecBlocks) {
		if (data.length != ecBlocks.totalByteCount()) {
		    throw new IllegalArgumentException("incompatible number of bytes");
        }

		return join(deinterleave(data, ecBlocks), ecBlocks);
	}
}
